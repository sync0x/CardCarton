# CardCarton

This Inkscape extension draws carton containers intended for plotter or dieline cutting and creasing.

This is release one and thus it is a proof of concept that developed while exploring the potential of the extensions framework.

For now, the blueprint to be rendered is dependent on which tab you have selected. The extension now does, however, replace its own layers of its own work in-place at last, so there is no need to Ctrl+Z (Undo) when reapplying changed parameters.

The finished product will allow you to compare current values with what you have selected previously, and this will be the purpose of the "Previous appearance fader" at the top of the dialog; to transition between the image of current and previous parameters entered via 16 steps of translucency.

One thing I do want to make light of, is that for all that is said of the consequences of artificial intelligence, we all know our computers cannot think for us; for the sake of our evolution it is not their place to even try. Relative to your creative workflow I, the developer, am blind; thus, if no one says anything to me about a need this script cannot fill, I am not going to know about it.

Out of necessity, I have had to do away with a continuous cut curve because the order of lid and flap components down the bottom is presently jumbled up. I will always aim to draw both my crease and cut outlines in a clockwise fashion.

Take a look at what debugging options #3 and #4 can do; the plotting debug options.
